module BackprojectionSar

using LinearAlgebra
using FFTW
using DSP
using Interpolations
using FLoops
using PaddedViews

export Platform, FmcwSensor, SarSystem, Target
export filtered_backprojection, range_migration

abstract type RadarSensor end

struct FmcwSensor <: RadarSensor
    fstart
    fstop
    Tp
    fs
end

function baseband(sensor::FmcwSensor, r; c=3e8)
    # propagation delay
    td = 2 * r / c
    # resulting signal phase
    # desired signal phase
    phi = -2 .* pi .* td .* (sensor.fstart .+ slope(sensor) .* fasttime(sensor))
    # complex sinusoid
    exp.(1im .* phi)
end

function fm(sensor::FmcwSensor)
    0.5 * (sensor.fstart + sensor.fstop)
end

function slope(sensor::FmcwSensor)
    (sensor.fstop - sensor.fstart) / sensor.Tp
end

function Nsamples(sensor::FmcwSensor)
    Int(sensor.fs * sensor.Tp)
end

function fasttime(sensor::FmcwSensor)
    range(0, sensor.Tp, length=Nsamples(sensor))
end

struct Platform
    start::Vector
    direction::Vector
    speed
end

function position(p::Platform, t)
    p.start .+ t .* p.speed .* p.direction
end

struct Target
    position::Vector
    reflectivity
end

struct SarSystem{T <: RadarSensor}
    platform::Platform
    sensor::T
    pri
end

function simulate(system::SarSystem, targets::Vector{Target}, N)
    # number of time samples
    M = Nsamples(system.sensor)
    # preallocate array to hold radar baseband signal
    signal = zeros(Complex{Float64}, N, M)

    for n in 1:N
        p = position(system.platform, n*system.pri)
        for target in targets
            # distance from platform to target
            r = norm(target.position - p)
            signal[n, :] .+= baseband(system.sensor, r)
        end
    end
    return signal
end

function filtered_backprojection(system::SarSystem{FmcwSensor}, baseband, xs, ys; c=3e8)
    # pad the baseband signals with zeros to reduce errors
    # due to range interpolation
    rcom = zeros(eltype(baseband),
                   size(baseband, 1),
                   Int(4*size(baseband, 2)))

    rcom[:,1:size(baseband,2)] = baseband

    # Ramp filter
    # bin frequencies
    freq = FFTW.fftfreq(size(rcom, 2), system.sensor.fs)
    # apply ramp filter
    rcom .*= transpose(abs.(freq))/system.sensor.fstart
    # windowing in range direction
    rcom .*= transpose(hanning(size(rcom, 2)))
    # perform range compression
    ifft!(rcom, 2)

    # windowing in azimuth direction
    rcom .*= hanning(size(rcom, 1))

    # these are the indices of positive range bins
    # b/c negative ranges don't make sense
    pos = 1:Int(size(rcom, 2)/2)
    # the fast time == signal propagation delay
    freq /= slope(system.sensor)
    fasttime = view(freq, pos)

    # platform positions at every measurement point
    positions = map(k -> position(system.platform, k*system.pri), 1:size(rcom, 1))

    # compensation function needed for FMCW radar systems
    fstart = system.sensor.fstart
    arclen = system.pri * system.platform.speed
    fcomp(td) = exp(2im*pi*fstart*td) * arclen

    # use linear interpolation in range for integral evaluation
    itp = interpolate((1:size(rcom,1), fasttime), rcom[:, pos], (NoInterp(), Gridded(Linear())))

    # compute the image
    backprojection(itp, positions, xs, ys, fcomp, c=c)
end

function backprojection(itp, positions, xs, ys, fcomp; c=3e8)
    @floop for (k,q) in enumerate(positions)
        @init p = zeros(3)
        @init subimage = Matrix{Complex{Float64}}(undef, length(xs), length(ys))

        fill!(subimage, 0.0im)

        for (i,x) in enumerate(xs), (j,y) in enumerate(ys)
            # point coordinates in the image plane
            @inbounds p[1] = x
            @inbounds p[2] = y
            # range between radar and image point
            r = norm(p - q)
            # corresponding propagation delay
            td = 2*r/c
            # remove initial carrier phase and add contribution
            # of this radar pulse to image point
            @inbounds subimage[i, j] += itp(k, td) * fcomp(td)
        end

        @reduce(image .+= subimage)
    end
    image
end

function range_migration(system::SarSystem{FmcwSensor}, baseband; c=3e8, zero_padding=nothing)
    # perform zero padding if necessary
    if !isnothing(zero_padding)
        (Nx, Nr) = size(baseband)
        numpad = Int(ceil(zero_padding - Nx)/2)
        bb = Matrix(PaddedView(0, baseband, (zero_padding, Nr), (numpad,1)))
    else
        bb = baseband
    end

    # compute variables
    (Nx, Nr) = size(bb)
    # azimuth step size between collected range profiles
    dx = system.pri * system.platform.speed
    # fast time variable
    t = fasttime(system.sensor)
    # azimuth wavenumber
    Kx = pi * range(-1, 1, length=Nx) / dx
    # range wavenumber
    Kr = 4 * pi / c * (system.sensor.fstart .+ slope(system.sensor) * t)
    # equivalent y-wavenumber sampled on polar grid
    Ky = transpose(Kr).^2 .- Kx.^2
    Ky[Ky .< 0.0] .= 0.0
    Ky .= sqrt.(Ky)
    # y-wavenumber uniformly sampled on rectangular grid
    Ky_unif = range(minimum(Ky), maximum(Ky), length=length(Kr))

    # crossrange axis (x in m)
    xs = range(-0.5, 0.5, length=Nx) * Nx * dx
    # downrange axis (y in m)
    ys = 2 * pi * collect(0:Nr-1) / (maximum(Ky) - minimum(Ky))

    # transform data into the 2D spatial frequency domain
    fft!(bb, 1)
    spatialfreq = fftshift(bb, 1)

    # no reference function multiplication because we assume the image is
    # calculated relative to sensor coordinate system
    # spatialfreq .*= transpose(exp.(1im*refrange*Ky))

    # pre-allocate memory for the Stolt-interpolated data
    stolt = zeros(eltype(spatialfreq), size(spatialfreq, 1), size(spatialfreq, 2))
    # perform Stolt interpolation
    for i in 1:size(stolt, 1)
        itp = LinearInterpolation(Ky[i,:], spatialfreq[i,:], extrapolation_bc=0.0im)
        stolt[i,:] .= itp(Ky_unif)
    end
    # apply windowing functions in azimuth and range direction
    stolt .*= hanning(Nx) .* transpose(hanning(Nr))
    # compress the spatial frequency data into the final image
    ifft!(stolt)

    (xs, ys, stolt)
end

end # module
