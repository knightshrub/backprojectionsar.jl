module Testing

using HDF5
using FFTW
using DSP
using Plots

include("BackprojectionSar.jl")
using .BackprojectionSar

function read_data(fname="sardata.h5")
    fid = h5open(fname, "r")

    group = fid[first(keys(fid))]
    dset = read(group[first(keys(group))])
    # data was exported from numpy so it is transposed
    dset = permutedims(dset, reverse(1:ndims(dset)))

    c = read(attributes(group)["c"])
    fs = read(attributes(group)["fs"])
    fc = read(attributes(group)["fc"])
    B = read(attributes(group)["B"])
    T = read(attributes(group)["T"])
    v = read(attributes(group)["platformVelocity"])
    pri = 1/read(attributes(group)["pulseRepetitionRate"])
    asr = read(attributes(group)["_azimuthSampleRate"])

    close(fid)

    baseband = conj.(mapslices(hilbert, dset, dims=2))

    platform = Platform([1.0,0.0,3.0], [-1.0,0.0,0.0], v)
    fmcw = FmcwSensor(fc, fc+B, T, fs)
    system = SarSystem(platform, fmcw, pri)

    (system, baseband, c)
end

function bp_realdata(xs, ys)
    (system, baseband, c) = read_data()
    image = filtered_backprojection(system, baseband, xs, ys, c=c)
    (xs, ys, image)
end

function plot_bp_realdata(M, N)
    (x, y, image) = bp_realdata(M,N)
    plot_image(x, y, image)
end

function rm_realdata(zero_padding=nothing)
    (system, baseband, c) = read_data()
    (x, y, image) = range_migration(system, baseband, c=c, zero_padding=zero_padding)

    idx = 1:Int(length(y)/8)

    (x, y[idx], image[:, idx])
end

function plot_rm_realdata(zero_padding=nothing)
    (x, y, image) = rm_realdata(zero_padding)
    plot_image(x, y, view(image, size(image,1):-1:1, :))
end

function plot_image(xs, ys, image)
    pltimage = 20*log10.(abs.(transpose(image)))
    m = maximum(pltimage)
    #heatmap(xs, ys, pltimage, aspect_ratio=:equal, c=:grays, clim=(m-50,m))
    heatmap(xs, ys, pltimage, aspect_ratio=:equal, c=:grays)
end

function run_simulation(M, N)
    platform = Platform([-2.0,-4.0,0.0], [1.0,0.0,0.0], 0.1)
    fmcw = FmcwSensor(4e9, 6e9, 2e-3, 1024/1e-3)
    system = SarSystem(platform, fmcw, 0.1)

    targets = [
        Target([0.0,-1.0,0.0], 1.0),
        Target([0.0,0.0,0.0], 1.0),
        Target([0.0,1.0,0.0], 1.0),

        Target([-1.0,-1.0,0.0], 1.0),
        Target([-1.0,0.0,0.0], 1.0),
        Target([-1.0,1.0,0.0], 1.0),

        Target([1.0,-1.0,0.0], 1.0),
        Target([1.0,0.0,0.0], 1.0),
        Target([1.0,1.0,0.0], 1.0),
    ]

    baseband = simulate(system, targets, 400)

    xs = range(-2.0, 2.0, length=M)
    ys = range(-2.0, 2.0, length=N)
    image = filtered_backprojection(system, baseband, xs, ys)

    (xs, ys, image)
end

end
